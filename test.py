import requests
from lxml import etree
import re
alldata = []
alloweds = ['sbj.speiyou.com',
           'stj.speiyou.com', 'ty.speiyou.com', 'jn.speiyou.com', 'qd.speiyou.com',
           'sjz.speiyou.com', 'ssh.speiyou.com', 'snj.speiyou.com', 'su.speiyou.com', 'yt.speiyou.com',
           'zb.speiyou.com', 'wz.speiyou.com', 'sx.speiyou.com', 'yz.speiyou.com', 'zj.speiyou.com',
           'xz.speiyou.com', 'nt.speiyou.com', 'cz.speiyou.com', 'sgz.speiyou.com', 'ssz.speiyou.com',
           'hu.speiyou.com', 'fs.speiyou.com', 'dg.speiyou.com', 'zs.speiyou.com', 'swh.speiyou.com',
           'cs.speiyou.com', 'scd.speiyou.com', 'scq.speiyou.com', 'shz.speiyou.com', 'zz.speiyou.com',
           'sy.speiyou.com', 'sxa.speiyou.com'
           ]
for allowed in alloweds:
    init_url = 'http://%s/search/index'%allowed
    res = requests.get(init_url+'/subject:/grade:/level:bx/lesson:/term:/gtype:time')
    htmlEmt = etree.HTML(res.text)
    rlist = htmlEmt.xpath('//*[@id="search-term"]/dl[1]/dd/ul/li/a/@href') # 年级
    for l in rlist:
        gradelist = []
        res = re.findall('grade:([\s\S]*?)/', l)
        if res != ['']:
            i= res[0]
            url = init_url+'/subject:/grade:%s/level:bx/lesson:/term:/gtype:time'%i
            data = [i,[],[],[],[]]
            res = requests.get(url=url)
            htmlEmt = etree.HTML(res.text)
            rlist = htmlEmt.xpath('//*[@id="search-term"]/dl[2]/dd/ul/li/a/@href') # 学科
            for l in rlist:
                res = re.findall('subject:([\s\S]*?)/',l)
                if res != [''] :
                    data[1].append(res[0])

            rlist = htmlEmt.xpath('//*[@id="search-term"]/dl[3]/dd/ul/li/a/@href') # 班次
            for l in rlist:
                res = re.findall('level:([\s\S]*?)/',l)
                if res != [''] and res[0] != 'bx':
                    data[2].append(res[0])

            rlist = htmlEmt.xpath('//*[@id="search-term"]/dl[4]/dd/ul/li/a/@href') # 班级类型
            for l in rlist:
                res = re.findall('term:([\s\S]*?)/',l)
                if res != ['']:
                    data[3].append(res[0])

            rlist = htmlEmt.xpath('//*[@id="search-term"]/dl[5]/dd/ul/li/a/@href') # 授课类型
            for l in rlist:
                res = re.findall('lesson:([\s\S]*?)/',l)
                if res != ['']:
                    data[4].append(res[0])
            print(data)
            with open('urls.txt','a') as file:
                file.writelines((init_url+'/grade:%s/subject:%s/level:%s/lesson:%s/term:%s/gtype:time\n' % (i,a,  b,  d,c,) for a in data[1] for b in data[2] for c in data[3] for d in data[4]))

    # tr_list = htmlEmt.xpath("//ul[@class='textlarge22']/li")[1:]





















