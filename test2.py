import requests
from lxml import etree
import time
import re


import threading
import threadpool
import queue
class downloader(object):
    __manage_threading = None
    __msg_buffer = queue.Queue()
    __threading_queue = queue.Queue()
    __threading_num = 100
    pool = threadpool.ThreadPool(__threading_num)
    def __init__(self,msgs):
        for msg in msgs:
            # (func,url,queue,**kwargs)
            self.__msg_buffer.put(msg)
        if not self.__manage_threading:
            self.__manage_threading = threading.Thread(target=self.__manage_downloader)
            self.__manage_threading.start()

    def __manage_downloader(self):
        while not self.__msg_buffer.empty():
            msgs_temp = []
            count = self.__msg_buffer.qsize()
            while not self.__msg_buffer.empty():
                msgs_temp.append((None,{'msg':self.__msg_buffer.get()}))
            requests = threadpool.makeRequests(self.__requests, msgs_temp)
            [self.pool.putRequest(req) for req in requests]
            while count != 0:
                m = self.__threading_queue.get()
                m[1].put((m[0],m[2]))
                count -= 1
            self.pool.wait()
        self.__manage_threading = None

    def __requests(self,msg):
        try:
            # print(msg[2],msg[3])
            respond =msg[0](msg[2],**msg[3])
        except IndexError:
            respond = msg[0](msg[2])
        except Exception as e:
            print('->',e)
            respond = None
        self.__threading_queue.put((respond,msg[1],msg[4]))

    def is_free(self):
        return self.__manage_threading == None and self.__msg_buffer.empty()

headers = { 'Host': 'www.xueersi.com',
            'Connection': 'keep-alive',
            'Cache-Control': 'max-age=0',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding': 'deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.9'}
t = time.time()
import hashlib
count = 0
q = queue.Queue()
msgs = [(requests.get, q, 'https://www.xueersi.com/chu1-0-4', {'headers': headers},1),]
dler = downloader(msgs)
s = set()

def get_md5(url):
    if isinstance(url,bytes):
        m = hashlib.md5()
        m.update(url)
        return m.hexdigest()
    else:
        raise ValueError(__file__)
while not dler.is_free():
    r = q.get()
    if r[1] == 1:
        htmlEmt = etree.HTML(r[0].text)
        urls = htmlEmt.xpath('/html/body/div[2]/div[1]/div/dl[2]/dd/a/@href')
        msgs = [(requests.get, q, url, {'headers': headers}, 2) for url in urls]
        dler = downloader(msgs)

    if r[1] == 2:
        htmlEmt = etree.HTML(r[0].text)
        urls = htmlEmt.xpath('/html/body/div[2]/div[1]/div/dl[3]/dd/a/@href')
        msgs = [(requests.get, q, url, {'headers': headers}, 3) for url in urls][1:]
        dler = downloader(msgs)

    if r[1] == 3:
        htmlEmt = etree.HTML(r[0].text)
        urls = htmlEmt.xpath('/html/body/div[2]/div[1]/div/dl[4]/dd/a/@href')
        msgs = [(requests.get, q, url, {'headers': headers}, 4) for url in urls][1:]
        dler = downloader(msgs)

    if r[1] == 4:
        htmlEmt = etree.HTML(r[0].text)
        urls = htmlEmt.xpath('/html/body/div[2]/div[1]/div/dl[5]/dd/a/@href')
        msgs = [(requests.get, q, url, {'headers': headers}, 5) for url in urls][1:]
        dler = downloader(msgs)

    if r[1] == 5:
        htmlEmt = etree.HTML(r[0].text)
        urls = htmlEmt.xpath('/html/body/div[2]/div[1]/div/dl[6]/dd/a/@href')
        msgs = [(requests.get, q, url, {'headers': headers}, 6) for url in urls][1:]
        dler = downloader(msgs)

    if r[1] == 6:
        htmlEmt = etree.HTML(r[0].text)
        urls = htmlEmt.xpath('/html/body/div[2]/div[1]/div/dl[7]/dd/a/@href')[1:]
        with open('urls.txt', 'a') as file:
            for url in urls:
                m = get_md5(url.encode())
                if m not in s:
                    s.add(m)
                    count += 1
                    file.write(url+'\n')
                    print(count,url)

    # if dler.is_free():
    #     print('dler.is_free()')
    #     break


print('一共花费：',time.time()-t)























